CREATE TABLE IF NOT EXISTS `customer` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `customer_reference` VARCHAR(255) NOT NULL,
  `customer_name` VARCHAR(255) NOT NULL,
  `address_line_1` VARCHAR(255) NOT NULL,
  `address_line_2` VARCHAR(255),
  `town` VARCHAR(255),
  `county` VARCHAR(255),
  `country` VARCHAR(255) NOT NULL,
  `postcode` VARCHAR(255) NOT NULL,
   PRIMARY KEY(id)
);
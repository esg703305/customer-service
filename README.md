# Customer Service 

This is a sprint boot project and uses maven to build, please clone the repo and open the project in an IDE of your choice. 

Run the main method of `CustomerServiceApplication`. 

Alternatively, you can run the following command from a unix terminal which has mvn 3 installed 

`git clone https://gitlab.com/esg703305/customer-service.git` 

`mvn clean package`

`java -jar target/customer-service-0.0.1-SNAPSHOT.jar`

This should start up the spring boot application. 

I used postman to test the endpoints, attached screenshots for references 

![img.png](img.png)

![img_1.png](img_1.png)
package com.esg.customerservice.dto;

import java.util.Objects;

public record Customer(String customerReference,
                       String customerName,
                       String addressLine1,
                       String addressLine2,
                       String town,
                       String county,
                       String country,
                       String postcode) {

    public static com.esg.customerservice.dao.Customer transformToDAO(Customer customer) {
        return com.esg.customerservice.dao.Customer
                .builder()
                .customerReference(validateCustomerReference(customer.customerReference))
                .customerName(validateCustomerName(customer.customerName))
                .addressLine1(checkNullOrTrim(customer.addressLine1))
                .addressLine2(checkNullOrTrim(customer.addressLine2))
                .town(checkNullOrTrim(customer.town))
                .county(checkNullOrTrim(customer.county))
                .country(validateCountry(customer.country))
                .postcode(validatePostCode(customer.postcode))
                .build();
    }


    private static String validateCustomerReference(String customerReference) throws IllegalArgumentException {
        if(customerReference == null || customerReference.trim().length() < 1) {
            throw new IllegalArgumentException("Customer Reference must not be null");
        }

        return customerReference.trim();
    }

    private static String validateCustomerName(String customerName) {
        if(customerName == null || customerName.trim().length() == 0) {
            throw new IllegalArgumentException("Customer Name must not be null or blank");
        }
        String[] names = customerName.trim().split(" ");

        if(names.length < 2) {
            throw new IllegalArgumentException("Customer Name must be contain at least first name and last name with space between first and last names e.g John Doe");
        }

        return customerName.trim();

    }

    private static String validatePostCode(String postcode) {
        //TODO requires more research on what is a valid post code (regex pattern)
        if(postcode == null || postcode.trim().length() == 0) {
            throw new IllegalArgumentException("postcode must not be null or blank");
        }

        return postcode.trim();
    }

    private static String validateCountry(String country) {
        //TODO requires more research on what is a valid post code (regex pattern) for each possible country;
        if(country == null || country.trim().length() == 0) {
            throw new IllegalArgumentException("country must not be null or blank");
        }

        return country.trim();
    }

    private static String checkNullOrTrim(String string) {
        if(string != null) {
            return string.trim();
        }

        return null;
    }
}

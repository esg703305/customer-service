package com.esg.customerservice.dao;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.extern.jackson.Jacksonized;

@Entity
@Table(name = "customer")
@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Jacksonized
public class Customer {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;
    @Column(name = "customer_reference")
    private String customerReference;
    @Column(name = "customer_name")
    private String customerName;
    @Column(name = "address_line_1")
    private String addressLine1;
    @Column(name = "address_line_2")
    private String addressLine2;
    @Column(name = "town")
    private String town;
    @Column(name = "county")
    private String county;
    @Column(name = "country")
    private String country;
    @Column(name = "postcode")
    private String postcode;

    public com.esg.customerservice.dto.Customer transformToDTO() {
        return new com.esg.customerservice.dto.Customer(
                this.customerReference,
                this.customerName,
                this.addressLine1,
                this.addressLine2,
                this.town,
                this.county,
                this.country,
                this.postcode);
    }

}

package com.esg.customerservice.controller;

import com.esg.customerservice.dto.Customer;
import com.esg.customerservice.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/customer")
public class CustomerServiceController {
    @Autowired private CustomerService customerService;
    @PostMapping("")
    public ResponseEntity<String> saveCustomerRecord(@RequestBody Customer customer) {
        customerService.saveCustomerRecord(Customer.transformToDAO(customer));
        return ResponseEntity.ok().body("ok");
    }

    @GetMapping("/{customer_ref}")
    public ResponseEntity<List<Customer>> getCustomers(@PathVariable("customer_ref") String customerReference) {
        var customers = customerService.getCustomersByCustomerReference(customerReference);
        return ResponseEntity.ok().body(customers
                .stream().map(com.esg.customerservice.dao.Customer::transformToDTO).toList());
    }

}

package com.esg.customerservice.service;

import com.esg.customerservice.dao.Customer;
import com.esg.customerservice.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerService {
    @Autowired
    private CustomerRepository customerRepository;

    public void saveCustomerRecord(Customer customer) {
        customerRepository.save(customer);
    }

    public List<Customer> getCustomersByCustomerReference(String customerReference) {
        return customerRepository.findAllByCustomerReference(customerReference);
    }
}

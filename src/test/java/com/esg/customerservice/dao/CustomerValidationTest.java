package com.esg.customerservice.dao;

import com.esg.customerservice.dto.Customer;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class CustomerValidationTest {

    @Test
    public void nullCustomerReference() {
        Customer customer = new Customer(null, null, null, null, null, null, null, null);
        try {
            Customer.transformToDAO(customer);
            fail();
        } catch (Exception e) {
            assertEquals("Customer Reference must not be null", e.getMessage());
        }

    }

    @Test
    public void emptyCustomerReference() {
        Customer customer = new Customer("", null, null, null, null, null, null, null);
        try {
            Customer.transformToDAO(customer);
            fail();
        } catch (Exception e) {
            assertEquals("Customer Reference must not be null", e.getMessage());
        }

    }

    @Test
    public void spacedCustomerReference() {
        Customer customer = new Customer("       ", null, null, null, null, null, null, null);
        try {
            Customer.transformToDAO(customer);
            fail();
        } catch (Exception e) {
            assertEquals("Customer Reference must not be null", e.getMessage());
        }

    }

    @Test
    public void validCustomer() {
        Customer customer = new Customer(
                "customer_reference",
                "customer name", "10 Downing Street",
                null, "London", "Greater London",
                "United Kingdom",
                "SW1A 2AA");

        com.esg.customerservice.dao.Customer customer1 = Customer.transformToDAO(customer);
        assertEquals(customer1.getCustomerReference(), customer.customerReference());
        assertEquals(customer1.getCustomerName(), customer.customerName());
        assertEquals(customer1.getAddressLine1(), customer.addressLine1());
        assertEquals(customer1.getAddressLine2(), customer.addressLine2());
        assertEquals(customer1.getTown(), customer.town());
        assertEquals(customer1.getCounty(), customer.county());
        assertEquals(customer1.getCountry(), customer.country());
        assertEquals(customer1.getPostcode(), customer.postcode());
    }

    @Test
    public void validCustomerTrimmed() {
        com.esg.customerservice.dao.Customer reference = Customer.transformToDAO(new Customer(
                "customer_reference",
                "customer name", "10 Downing Street",
                null, "London", "Greater London",
                "United Kingdom",
                "SW1A 2AA"));

        Customer customer = new Customer(
                " customer_reference ",
                "customer name", " 10 Downing Street ",
                null, " London ", " Greater London  ",
                " United Kingdom ",
                " SW1A 2AA ");

        com.esg.customerservice.dao.Customer customer1 = Customer.transformToDAO(customer);
        assertEquals(reference.getCustomerReference(), customer1.getCustomerReference());
        assertEquals(reference.getCustomerName(), customer1.getCustomerName());
        assertEquals(reference.getAddressLine1(), customer1.getAddressLine1());
        assertEquals(reference.getAddressLine1(), customer1.getAddressLine1());
        assertEquals(reference.getTown(), customer1.getTown());
        assertEquals(reference.getCounty(), customer1.getCounty());
        assertEquals(reference.getCountry(), customer1.getCountry());
        assertEquals(reference.getPostcode(), customer1.getPostcode());
    }

    @Test
    public void nullCustomerName() {
        Customer customer = new Customer(
                "customer_reference",
                null, "10 Downing Street",
                null, "London", "Greater London",
                "United Kingdom",
                "SW1A 2AA");

        try {
            Customer.transformToDAO(customer);
            fail();
        } catch (Exception e) {
            assertEquals("Customer Name must not be null or blank", e.getMessage());
        }
    }

    @Test
    public void emptyCustomerName() {
        Customer customer = new Customer(
                "customer_reference",
                "", "10 Downing Street",
                null, "London", "Greater London",
                "United Kingdom",
                "SW1A 2AA");

        try {
            Customer.transformToDAO(customer);
            fail();
        } catch (Exception e) {
            assertEquals("Customer Name must not be null or blank", e.getMessage());
        }
    }

    @Test
    public void spacedCustomerName() {
        Customer customer = new Customer(
                "customer_reference",
                "     ", "10 Downing Street",
                null, "London", "Greater London",
                "United Kingdom",
                "SW1A 2AA");

        try {
            Customer.transformToDAO(customer);
            fail();
        } catch (Exception e) {
            assertEquals("Customer Name must not be null or blank", e.getMessage());
        }
    }

    @Test
    public void singleName() {
        Customer customer = new Customer(
                "customer_reference",
                "Johnson", "10 Downing Street",
                null, "London", "Greater London",
                "United Kingdom",
                "SW1A 2AA");

        try {
            Customer.transformToDAO(customer);
            fail();
        } catch (Exception e) {
            assertEquals("Customer Name must be contain at least first name and last name with space between first and last names e.g John Doe", e.getMessage());
        }
    }



    @Test
    public void nullCountry() {
        Customer customer = new Customer(
                "customer_reference",
                "customer name", null,
                null, null, null,
                null,
                "SW1A 2AA");

        try {
            Customer.transformToDAO(customer);
            fail();
        } catch (Exception e) {
            assertEquals("country must not be null or blank", e.getMessage());
        }
    }

    @Test
    public void emptyCountry() {
        Customer customer = new Customer(
                "customer_reference",
                "customer name", null,
                null, null, null,
                "",
                "SW1A 2AA");

        try {
            Customer.transformToDAO(customer);
            fail();
        } catch (Exception e) {
            assertEquals("country must not be null or blank", e.getMessage());
        }
    }
    @Test
    public void spacedCountry() {
        Customer customer = new Customer(
                "customer_reference",
                "customer name", null,
                null, null, null,
                "     ",
                "SW1A 2AA");

        try {
            Customer.transformToDAO(customer);
            fail();
        } catch (Exception e) {
            assertEquals("country must not be null or blank", e.getMessage());
        }
    }


    @Test
    public void nullPostcode() {
        Customer customer = new Customer(
                "customer_reference",
                "customer name", null,
                null, null, null,
                "United Kingdom",
                null);

        try {
            Customer.transformToDAO(customer);
            fail();
        } catch (Exception e) {
            assertEquals("postcode must not be null or blank", e.getMessage());
        }
    }

    @Test
    public void emptyPostcode() {
        Customer customer = new Customer(
                "customer_reference",
                "customer name", null,
                null, null, null,
                "United Kingdom",
                "");

        try {
            Customer.transformToDAO(customer);
            fail();
        } catch (Exception e) {
            assertEquals("postcode must not be null or blank", e.getMessage());
        }
    }
    @Test
    public void spacedPostcode() {
        Customer customer = new Customer(
                "customer_reference",
                "customer name", null,
                null, null, null,
                "United Kingdom",
                "  ");
        try {
            Customer.transformToDAO(customer);
            fail();
        } catch (Exception e) {
            assertEquals("postcode must not be null or blank", e.getMessage());
        }
    }


}

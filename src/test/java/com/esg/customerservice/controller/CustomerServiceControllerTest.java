package com.esg.customerservice.controller;

import com.esg.customerservice.dto.Customer;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@SpringBootTest
class CustomerServiceControllerTest {
    @Autowired
    private CustomerServiceController controller;
    @Autowired
    private ObjectMapper mapper;
    private MockMvc mockMvc;

    @BeforeEach
    void beforeEach() {
        mockMvc = MockMvcBuilders.standaloneSetup(controller)
                .setControllerAdvice(new ServiceControllerAdvice())
                .build();
    }

    @Test
    void saveCustomerAndGetCustomer() throws Exception {
        Customer customer = new Customer(
                "customer_reference",
                "customer name", "10 Downing Street",
                null, "London", "Greater London",
                "United Kingdom",
                "SW1A 2AA");

        String customerExpected1 = mapper.writeValueAsString(customer);

        MockHttpServletResponse response = mockMvc.perform(post("/api/customer")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(customerExpected1)
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();
        assertEquals(HttpStatus.OK.value(), response.getStatus());
        assertEquals("ok", response.getContentAsString());

        Customer customer2 = new Customer(
                "customer_reference",
                "John Doe", "10 Downing Street",
                null, "London", "Greater London",
                "United Kingdom",
                "SW1A 2AA");

        String customerExpected2 = mapper.writeValueAsString(customer2);

        mockMvc.perform(post("/api/customer")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(customerExpected2)
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();


        response = mockMvc.perform(get("/api/customer/customer_reference")
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();
        assertEquals(HttpStatus.OK.value(), response.getStatus());
        assertEquals("[" + customerExpected1 + "," + customerExpected2 + "]", response.getContentAsString());
    }

}
